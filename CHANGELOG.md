
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:25PM

See merge request itentialopensource/adapters/adapter-thruk_monitoring!13

---

## 0.4.3 [09-18-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-thruk_monitoring!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:36PM

See merge request itentialopensource/adapters/adapter-thruk_monitoring!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:48PM

See merge request itentialopensource/adapters/adapter-thruk_monitoring!9

---

## 0.4.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-thruk_monitoring!8

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_13:15PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-thruk_monitoring!7

---

## 0.3.3 [03-12-2024]

* Changes made at 2024.03.12_10:59AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-thruk_monitoring!6

---

## 0.3.2 [02-27-2024]

* Changes made at 2024.02.27_11:34AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-thruk_monitoring!5

---

## 0.3.1 [01-05-2024]

* more migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-thruk_monitoring!4

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-thruk_monitoring!2

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-thruk_monitoring!1

---

## 0.1.1 [07-29-2021]

- Initial Commit

See commit 62117a8

---
